FROM ubuntu:latest

# Install cron
RUN apt-get update
RUN apt-get install cron

COPY cronjob /etc/cron.d/cronjob

# Add crontab file in the cron directory
RUN crontab /etc/cron.d/cronjob

# Give execution rights on the cron job
RUN chmod 0644 /etc/cron.d/cronjob

# Create the log file to be able to run tail
RUN touch /var/log/cron.log

# Run the command on container startup
CMD cron && tail -f /var/log/cron.log