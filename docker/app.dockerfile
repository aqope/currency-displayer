FROM php:7.1.3-fpm

RUN apt-get update && apt-get install -y cron libmcrypt-dev \
    mysql-client libmagickwand-dev --no-install-recommends \
    && pecl install imagick \
    && docker-php-ext-enable imagick \
    && docker-php-ext-install mcrypt pdo_mysql

COPY cronjob /etc/cron.d/cronjob

RUN crontab /etc/cron.d/cronjob

RUN chmod 0644 /etc/cron.d/cronjob

RUN touch /var/log/cron.log

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN groupadd -g 1000 www

RUN useradd -u 1000 -ms /bin/bash -g www www

COPY --chown=www:www . /var/www

#USER www
COPY startup.sh /startup.sh

RUN chmod +x /startup.sh

#ENTRYPOINT ["bash", "-c"]

EXPOSE 9000

CMD ["/startup.sh"]