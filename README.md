# Currency Displayer

### How to set up project
1. Install composer dependencies:
    * cd ~/src/
    * docker run --rm -v $(pwd):/app composer install
2. Copy environment files for the Docker and Laravel
    * cp .env.example .env
    * cp src/.env.example src/.env
3. Configure prefered ports for Docker and Laravel (or leave default)
4. Build containers and put the up
    * docker-compose up
5. Execute migrations
    * docker exec -it app bash
    * cd /var/www/
    * php artisan migrate
6. Open http://localhost:8080/ and observe functionality

Cron is set up to be ran in the "app" container and set to run every minute (for demo purposes). Command output can be seen in storage/logs/api-output.log .

---

### Contributors
Artur Paklin

