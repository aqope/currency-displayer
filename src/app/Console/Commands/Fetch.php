<?php
/**
 * @author Artur Paklin (paklin.artur@gmail.com)
 */
namespace App\Console\Commands;

use App\Models\Currency;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class Fetch extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'api:fetch 
                            {--reset : Reset data and perform initial import.} 
                            {--reimport-today : Re-import todays values.}
                            {--import-date= : Import specific date, please use day/month/year format.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch currency';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Pull data from the API
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('reset')) {
            Currency::query()->truncate();
        }

        if ($this->option('reimport-today')) {
            Currency::query()->where('date', '=', date('d/m/Y'))
                ->delete();
        }

        $data = "";
        try {
            $client = new Client();
            $resource = $client->request('GET', config('api.url'));

            if ($resource->getStatusCode() === 200) {
                $data = new \SimpleXMLElement($resource->getBody()->getContents());
            }
        } catch(GuzzleException $ex) {
            echo "Error occurred while fetching data... \n";
            Log::error($ex);
        }

        if ($this->parseData($data)) {
            echo "Successfully parsed data. \n";
        } else {
            echo "Failed to parsed data. \n";
        }
    }

    /**
     * Parse data and insert into the database
     *
     * @param \SimpleXMLElement $data
     * @return bool
     */
    protected function parseData(\SimpleXMLElement $data): bool
    {
        $availableDates = Currency::all('date')
            ->pluck('date')
            ->unique()
            ->map(function($item) {
                return strtotime(str_replace('/', '-', $item));
            })
            ->toArray();

        try {
            $collection = [];
            foreach ($data->channel->item as $item) {
                $codes = [];
                preg_match_all('(\w+ \d+.\d+)', (string)$item->description, $codes);
                if (in_array(strtotime($item->pubDate), $availableDates)) {
                    echo "Skipping {$item->pubDate}...\n";
                    continue;
                }

                foreach ($codes[0] as $code) {
                    $curr = explode (' ', $code);
                    $currency = [
                        'name' => $curr[0],
                        'value' => (float)$curr[1],
                        'date' => date('d/m/Y', strtotime($item->pubDate))
                    ];

                    $collection[] = $currency;
                }
                echo "Fetched {$item->pubDate}...\n";
            }
            Currency::query()->insert($collection);
        } catch (\Exception $ex) {
            echo "Error occurred during parsing... \n";
            Log::error($ex);
            return false;
        }

        return true;
    }
}
