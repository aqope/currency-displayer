<?php
/**
 * @author Artur Paklin (paklin.artur@gmail.com)
 */
namespace App\Http\Controllers;

use App\Models\Currency;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    protected $collection;

    protected $page;

    protected $pagination;

    protected $availableDates;

    protected function prepareCollection($date)
    {
        $this->collection = Currency::where('date', '=', date('d/m/Y' ,$date))
            ->paginate($this->pagination);
    }

    protected function getAvailableDates()
    {
        if (empty($availableDates)) {
            $availableDates = Currency::all('date')
                ->pluck('date')
                ->unique()
                ->map(function($item) {
                    return strtotime(str_replace('/', '-', $item));
                });
        }

        return $availableDates;
    }

    protected function getPaginations()
    {
        return [10, 15, 25];
    }

    public function index(Request $request)
    {
        $this->pagination = $request->input('pagination') ?? 25;
        $date = $request->input('date') ?? $this->getAvailableDates()->sortBy('date')->first();
        $this->prepareCollection($date);

        return view(
            'main',
            [
                'collection' => $this->collection,
                'dates' => $this->getAvailableDates(),
                'currentDate' => $date,
                'page' => $this->collection->currentPage(),
                'pages' => $this->collection->lastPage(),
                'paginations' => $this->getPaginations(),
                'currentPagination' => $this->pagination
            ]
        );
    }
}
