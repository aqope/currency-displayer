<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body>
<div class="flex-center position-ref full-height">
    <nav class="navbar navbar-light bg-light">
        <a class="navbar-brand" href="#">
            <img src="{{ asset('img/logo2.png') }}" width="30" height="30" class="d-inline-block align-top" alt="">
            Currency Viewer
        </a>
    </nav>

    <div class="container">
        <div class="info mt-4">
            <p class="text-right">{{ $page }} of {{ $pages }} pages</p>
        </div>
        <div class="row">
            <div class="col">
                <span class="float-left ml-1">Date</span>
            </div>
            <div class="col">
                <span class="float-right mr-2">Pagination</span>
            </div>
        </div>
        <div class="row">
            <div class="col d-flex justify-content-start">
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ date('d/m/Y', $currentDate) }}
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        @foreach ($dates as $date)
                            <a class="dropdown-item" href="{{ url('?pagination=' . $currentPagination . '&date=' . $date) }}">{{ date('d/m/Y', $date) }}</a>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col d-flex justify-content-end">
                @foreach ($paginations as $pagination)
                    <a href="{{ url('?pagination=' . $pagination . '&date=' . $currentDate) }}" class="btn btn-link {{ $pagination == $currentPagination ? 'disabled' : '' }}">{{ $pagination }}</a>
                @endforeach
            </div>
        </div>
        <div class="row mb-2 mt-2">
            @if ($page - 1 > 0)
                <div class="col d-flex justify-content-start">
                    <a href="{{ url('?page=' . ($page - 1)) . '&pagination=' . $currentPagination . '&date=' . $currentDate }}" class="text-left"><button type="button" class="btn btn-light">Previous Page</button></a>
                </div>
            @endif
            @if ($page + 1 <= $pages)
                <div class="col d-flex justify-content-end">
                    <a href="{{ url('?page=' . ($page + 1)) . '&pagination=' . $currentPagination . '&date=' . $currentDate }}" class="text-right"><button type="button" class="btn btn-light">Next Page</button></a>
                </div>
            @endif
        </div>
        <table class="table table-stripped table-hover">
            <caption>List of currencies values up to {{ date('d/m/Y', $currentDate) }}.</caption>
            <thead class="thead-light">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Code</th>
                    <th scope="col">Value (€)</th>
                    <th scope="col">Date</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($collection as $item)
                <tr>
                    <th scope="row">{{ $item->id }}</th>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->value }}</td>
                    <td>{{ $item->date }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
</body>
</html>
